import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    bread: []
  },
  mutations: {
    set_bread(state, value) {
      state.bread = value
      console.log(state.bread)
    }
  },
  actions: {
  },
  modules: {

  }
})
