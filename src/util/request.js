//1. 导入axios对象
import axios from "axios";
// import store from '@/store/index'
//2. 用axios创建一个axios的实例
const Server = axios.create({
    baseURL: "https://www.liulongbin.top:8888/api/private/v1/",//根域名
    timeout: 3000,//超时时间
});


//3. 定义请求拦截器
Server.interceptors.request.use((config) => {
    let token = JSON.parse(sessionStorage.getItem('zgj_token'))
    if (token != null) {
        // console.log(token.token)
        config.headers.Authorization = token.token
    }
    return config;
}, (error) => {


    return Promise.reject(error);
});

//4. 定义相应拦截器
Server.interceptors.response.use((response) => {

    if (response.status == 200) {

        return response.data;
    }
}, (error) => {
    return Promise.reject(error);
});


//5. 定义好的对象抛出对象

export default Server;