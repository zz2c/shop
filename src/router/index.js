import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    name: 'login',
    component: () => import('../views/admin/login.vue')
  },
  {
    path: '/home',
    name: 'home',
    component: () => import('../views/admin/home.vue'),
    redirect: '/index',
    children: [
      {
        path: '/users',
        name: 'users',
        component: () => import('../views/admin/users.vue'),
        meta: ['首页', '用户管理', '用户列表']
      },
      {
        path: '/index',
        name: 'index',
        component: () => import('../views/admin/index.vue'),
      },
      {
        path: '/roles',
        name: 'roles',
        component: () => import('../views/admin/roles.vue'),
        meta: ['首页', '权限管理', '角色列表']
      },
      {
        path: '/rights',
        name: 'rights',
        component: () => import('../views/admin/rights.vue'),
        meta: ['首页', '权限管理', '权限列表']
      },
      {
        path: '/goods',
        name: 'goods',
        component: () => import('../views/admin/goods.vue'),
        meta: ['首页', '商品管理', '商品列表'],

      },
      {
        path: '/goods/add',
        name: 'goods/add',
        component: () => import('../views/admin/add.vue'),
        meta: ['首页', '商品管理', '添加']
      },
      {
        path: '/reports',
        name: 'reports',
        component: () => import('../views/admin/reports.vue'),
        meta: ['首页', '商品管理', '添加']
      },
      {
        path: '/orders',
        name: 'orders',
        component: () => import('../views/admin/orders.vue'),
        meta: ['首页', '商品管理', '添加']
      },
      {
        path: '/categories',
        name: 'categories',
        component: () => import('../views/admin/categories.vue'),
        meta: ['首页', '商品管理', '添加']
      },
      {
        path: '/params',
        name: 'params',
        component: () => import('../views/admin/params.vue'),
        meta: ['首页', '商品管理', '添加']
      },
    ]
  },
]

const router = new VueRouter({
  routes
})

export default router
