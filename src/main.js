import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入element-ui
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import axios from '@/util/request.js'
// import f from '@/views/methods/methods.js'
// Vue.prototype.f=f
Vue.prototype.$axios = axios;
Vue.use(ElementUI);
// 引入element-ui

import echarts from 'echarts'

Vue.prototype.$echarts = echarts
//导入vue-quill-editor（富文本编辑器）
import VueQuillEditor from 'vue-quill-editor'
//导入vue-quill-editor的样式
import 'quill/dist/quill.core.css'
import 'quill/dist/quill.snow.css'
import 'quill/dist/quill.bubble.css'


import TreeTable from 'vue-table-with-tree-grid'
Vue.config.productionTip = false
Vue.component('tree-table', TreeTable)



//全局注册组件
// Vue.component('tree-table', TreeTable)
//全局注册富文本组件
Vue.use(VueQuillEditor)

// 路由守卫
router.beforeEach((to, from, next) => {
  console.log(to)
  let token = sessionStorage.getItem('zgj_token')
  if (to.path == '/login') {
    if (token != null) {
      next('/home')
    } else {

    }
  }
  if (to.path == '/home') {
    if (token != null) {

    } else {
      next('/login')
    }
  }
  next()

  // 面包屑
  if (to.hasOwnProperty('meta')) {
    store.commit('set_bread', to.meta)
    // console.log(to.meta)
  }
  // 面包屑


})
// 路由守卫

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
